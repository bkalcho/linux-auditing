#!/bin/bash
#: Title	: network_config_audit.sh
#: Date		: 2018-08-27
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Audit basic components of network configuration, including:
#:+		:	*Network interfaces
#:+		:	*IP address
#:+		:	*Netmask
#:+		:	*Gateway
#:+		:	*DNS configuration
#:+		:	*Hostname
#: Options	: None

function repeatChars() {
	## Function which prints defined number of specific characters
	if [ $# -ne 2 ]; then
		echo "USAGE: ${0##*/} number-of-repetitions char-or-string"
		exit 1
	fi
	comm=$(printf "%$1s")
	echo "${comm// /$2}"
	return 0
}


## Get names of all interfaces and their assigned IP addresses if any
echo "Starting auditing of network interfaces..."
## AWK will search for lines with 'number:' or which starts with 'inet' and
## follows with ip address in CIDR notation
ip addr | awk '{ print $1,$2 }' |  awk 'BEGIN { print "########################"\
	"#####\n*** Available interfaces: ***\n#############################\n" } \
	/^[[:digit:]]:.*:$|inet\s([0-9]{1,3}\.){3}[0-9]{1,3}/ \
	{ printf "%s %s\n", $1, $2 } END { print "\n" }'


## Get existing interface's statuses
echo "Getting interface's state..."
repeatChars 39 "#"
echo "*** Local network interface's state ***"
repeatChars 39 "#"
echo
nmcli device status


echo -e "\nGetting routing configuration..."
repeatChars 27 "#"
echo "*** Routing information ***"
repeatChars 27 "#"
echo
ip route


echo -e "\nGetting default search domain and DNS servers..."
repeatChars 25 "#"
echo "*** DNS Configuration ***"
repeatChars 25 "#"
echo
## AWK will print one string if it encounters 'Search' string and another if encounteres 'nameserver' string
cat /etc/resolv.conf | grep -v Generated | sed 's/search/Search Domain:/' | awk '{ if($1=="Search") {print $1, $2, $3} else if($1="nameserver") { print "DNS" NR-1 ":", $2} }'


echo -e "\nGetting local machine information including its static hostname..."
repeatChars 35 "#"
echo "*** System hostname information ***"
repeatChars 35 "#"
echo
hostnamectl status
